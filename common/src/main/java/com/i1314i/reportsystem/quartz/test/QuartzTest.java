package com.i1314i.reportsystem.quartz.test;

import com.i1314i.reportsystem.quartz.ram.RAMJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

/**
 * @author 平行时空
 * @created 2018-09-23 16:41
 **/
public class QuartzTest {
    public static void main(String[] args) throws SchedulerException {
        SchedulerFactory schedulerFactory=new StdSchedulerFactory();
        Scheduler scheduler=schedulerFactory.getScheduler();
        scheduler.start();
        // define the job and tie it to our HelloJob class
        JobDetail job =  JobBuilder.newJob(RAMJob.class)
                .withIdentity("myJob", "group1")
                .build();


        // 触发器类型
        SimpleScheduleBuilder builder = SimpleScheduleBuilder
                // 设置执行次数
                .repeatSecondlyForTotalCount(5);

        // Trigger the job to run now, and then every 40 seconds
        Trigger trigger =  TriggerBuilder.newTrigger()
                .withIdentity("myTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(40)
                        .repeatForever())
                .build();

        // Tell quartz to schedule the job using our trigger
        scheduler.scheduleJob(job, trigger);
     //   scheduler.shutdown();

    }
}

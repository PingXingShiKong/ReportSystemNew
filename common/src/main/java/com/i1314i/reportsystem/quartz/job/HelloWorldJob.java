package com.i1314i.reportsystem.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 平行时空
 * @created 2018-09-25 20:42
 **/
public class HelloWorldJob  implements Job{
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd : HH:mm:ss");
        System.out.println("hello world"+dateFormat.format(new Date()));
    }
}

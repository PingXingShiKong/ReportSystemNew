package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.ResultMap;
import com.i1314i.reportsystem.po.User;

/**
 * @author 平行时空
 * @created 2018-09-18 17:32
 **/
public interface UserMapper {
    User selectUserByUserName(String username);
    void insertUser(User user);

}

package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Role;

import java.util.List;

/**
 * @author 平行时空
 * @created 2018-09-17 23:30
 **/
public interface RoleMapper {
    void insertRole(Role role);
    Role selectRoleById(String roleId);
    List<Role> getRolePermission(String role);
}

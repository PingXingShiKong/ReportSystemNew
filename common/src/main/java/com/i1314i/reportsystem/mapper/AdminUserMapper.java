package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.AdminUser;

/**
 * @author 平行时空
 * @created 2018-09-21 14:38
 **/
public interface AdminUserMapper {
    AdminUser selectAdminUserByUserName(String username);
}

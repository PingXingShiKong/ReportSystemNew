package com.i1314i.reportsystem.po;

import java.io.Serializable;

/**
 * @author 平行时空
 * @created 2018-09-17 23:30
 **/
public class Role implements Serializable{
    private String roleid;
    private String role;
    private String permission;

    public Role(String roleid, String role, String permission) {
        this.roleid = roleid;
        this.role = role;
        this.permission = permission;
    }

    public Role() {
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}

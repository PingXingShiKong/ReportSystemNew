package com.i1314i.reportsystem.po;

import java.io.Serializable;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-09-18 17:30
 **/
public class User implements Serializable{
    private String userid;
    private String username;
    private String password;
    private String phone;
    private Integer status;
    private String roleid;
    private Role role;
    private List<Role> roleList;

    private String codetoken;
    private String code;

    public String getCodetoken() {
        return codetoken;
    }

    public void setCodetoken(String codetoken) {
        this.codetoken = codetoken;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public User() {
    }


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User(String userid, String username, String password, String phone, Integer status, String roleid, List<Role> roleList) {
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.status = status;
        this.roleid = roleid;
        this.roleList = roleList;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }



    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}

package com.i1314i.reportsystem.po;

import java.io.Serializable;

/**
 * @author 平行时空
 * @created 2018-08-28 21:59
 **/
public class Token implements Serializable {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}

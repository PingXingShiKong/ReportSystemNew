package com.i1314i.reportsystem.config;

/**
 * @author 平行时空
 * @created 2018-04-04 23:12
 **/

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;

@Configuration
@PropertySource(value= "classpath:dao.properties",encoding = "UTF-8")
@MapperScan("com.i1314i.reportsystem.mapper")
public class MybatisConfig {
    private final static Logger logger= LoggerFactory.getLogger(MybatisConfig.class);

    @Autowired
    private DataSource dataSource;



    /**
     * SqlSession工厂
     * @return
     * @throws PropertyVetoException
     * @throws IOException
     */
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean() throws PropertyVetoException, IOException {
        ResourcePatternResolver resourcePatternResolver=new PathMatchingResourcePatternResolver();
        SqlSessionFactoryBean sqlSessionFactoryBean =new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(resourcePatternResolver.getResources("classpath*:mapping/*.xml"));
        return sqlSessionFactoryBean;
    }

    //事务管理器
    @Bean(name="transactionManager")
    public DataSourceTransactionManager dataSourceTransactionManager() throws PropertyVetoException {
        DataSourceTransactionManager dataSourceTransactionManager=new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dataSource);
        return dataSourceTransactionManager;
    }

/*
    @Bean(name="transactionInterceptor")
    public TransactionInterceptor interceptor() throws PropertyVetoException {
        TransactionInterceptor interceptor=new TransactionInterceptor();

        interceptor.setTransactionManager(dataSourceTransactionManager());

        Properties attributes=new Properties();
        attributes.setProperty("save*","REQUIRED");
        attributes.setProperty("add*","REQUIRED");
        attributes.setProperty("create*","REQUIRED");
        attributes.setProperty("delete*","REQUIRED");
        attributes.setProperty("update*","REQUIRED");
        attributes.setProperty("find*","SUPPORTS,readOnly");
        attributes.setProperty("select*","SUPPORTS,readOnly");
        attributes.setProperty("get*","SUPPORTS,readOnly");
        interceptor.setTransactionAttributes(attributes);
        return interceptor;
    }

    */
}

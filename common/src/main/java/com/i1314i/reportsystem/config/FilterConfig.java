package com.i1314i.reportsystem.config;


import com.i1314i.reportsystem.filter.TokenFilter;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.shiro.authc.*;
import javax.servlet.Filter;
import java.util.HashMap;
import java.util.Map;
import org.apache.shiro.mgt.SecurityManager;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * @author 平行时空
 * @created 2018-09-08 17:31
 **/

//@Configuration
public class FilterConfig {

    @Bean
    public Filter tokenAuthorFilter(){
        return new TokenFilter();//自定义的过滤器
    }
//    @Bean
//    public Filter validatorFilter (){
//        return new ValidatorFilter();//自定义的过滤器
//    }




    @Bean
    public FilterRegistrationBean filterRegistrationBean1(){
        FilterRegistrationBean filterRegistrationBean=new FilterRegistrationBean();
        filterRegistrationBean.setFilter(tokenAuthorFilter());
        filterRegistrationBean.addUrlPatterns("/login/*","/data/*");
        filterRegistrationBean.setOrder(1);//order的数值越小 则优先级越高
        return filterRegistrationBean;
    }



//    @Bean
//    public FilterRegistrationBean filterRegistrationBean2(){
//        FilterRegistrationBean filterRegistrationBean=new FilterRegistrationBean();
//        filterRegistrationBean.setFilter(validatorFilter());
//        filterRegistrationBean.addUrlPatterns("*");
//        filterRegistrationBean.setOrder(3);
//        return filterRegistrationBean;
//    }


}

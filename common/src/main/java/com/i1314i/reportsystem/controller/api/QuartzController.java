package com.i1314i.reportsystem.controller.api;

import com.i1314i.reportsystem.po.ResultMap;
import com.i1314i.reportsystem.po.quartz.JobEntity;
import com.i1314i.reportsystem.service.quartz.QuartzService;
import com.i1314i.reportsystem.utils.inimsg.QuartzMsgUtils;
import com.i1314i.reportsystem.utils.jedisUtils.other.StringUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Quartz操作类
 * @author 平行时空
 * @created 2018-09-25 21:59
 **/
@RestController
@RequestMapping("/api/quartz")
//@RequiresRoles(logical = Logical.OR, value = {"admin"})
public class QuartzController {
    @Autowired
    private Scheduler quartzScheduler;
    @Autowired
    private QuartzService quartzService;
    @Autowired
    private ResultMap resultMap;

    /**
     * 获取定时任务job列表
     * @return
     */

    @RequestMapping(method = {RequestMethod.GET})
    public ResultMap listJob(){
        List<JobEntity> jobInfos=null;
        try {
             jobInfos = this.getSchedulerJobInfo();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return resultMap
                .code(QuartzMsgUtils.quartz_success_code)
                .msg(QuartzMsgUtils.quartz_success_msg)
                .data(jobInfos);

    }


    /**
     * 添加定时任务
     * @param jobEntity
     * @return
     */
    @RequestMapping(method = {RequestMethod.POST})
    public ResultMap add(@RequestBody JobEntity jobEntity){
        Class cls = null;
        try {
            cls = Class.forName(jobEntity.getJobClass());
            quartzService.addJob(jobEntity.getJobName(), jobEntity.getJobGroup(), jobEntity.getTriggerName(), jobEntity.getTriggerGroupName(), cls, jobEntity.getCronExpr());
        } catch (ClassNotFoundException e) {
           return resultMap.code(QuartzMsgUtils.quartz_error_code).msg(QuartzMsgUtils.quartz_error_msg);
        }
        return resultMap.code(QuartzMsgUtils.quartz_success_code).msg(QuartzMsgUtils.quartz_success_msg);
    }


    /**
     * {  JSON格式
     *      "oldjobName":"",
     *      "oldjobGroup":"",
     *      "oldtriggerName":"",
     *      "oldtriggerGroup":"",
     *      "jobName":"",
     *      "jobNroup":"",
     *      "triggerName":"",
     *      "triggergroupName":""
     *  }
     *
     * 编辑定时任务
     * @param jobEntity
     * @return
     */
    @RequestMapping(method = {RequestMethod.PUT})
    public ResultMap edit(@RequestBody JobEntity jobEntity){
        boolean result = quartzService.modifyJobTime(jobEntity.getOldjobName(), jobEntity.getOldjobGroup(), jobEntity.getOldtriggerName(), jobEntity.getOldtriggerGroup(),
                jobEntity.getJobName(), jobEntity.getJobGroup(), jobEntity.getTriggerName(), jobEntity.getTriggerGroupName(), jobEntity.getCronExpr());

        if (result){
            return resultMap
                    .code(QuartzMsgUtils.quartz_success_code)
                    .msg(QuartzMsgUtils.quartz_success_msg);
        }else {
            return resultMap
                    .code(QuartzMsgUtils.quartz_error_code)
                    .msg(QuartzMsgUtils.quartz_error_msg);
        }
    }


    /**
     * JSON格式
     *  {
     *       "jobName":"",
     *       "jobGroup":""
     *  }
     * 暂停定时任务
     *
     * @param jobEntity
     * @return
     */
    @RequestMapping(value = "/pause",method = {RequestMethod.PUT})
    public ResultMap pauseJob(@RequestBody JobEntity jobEntity){

        if(org.springframework.util.StringUtils.isEmpty(jobEntity.getJobName())|| org.springframework.util.StringUtils.isEmpty(jobEntity.getJobGroup())){
            return resultMap.code(QuartzMsgUtils.quartz_empty_code)
                    .msg(QuartzMsgUtils.quartz_empty_msg);
        }

            quartzService.pauseJob(jobEntity.getJobName(), jobEntity.getJobGroup());
        try{
        }catch (Exception e){
            return resultMap
                    .code(QuartzMsgUtils.quartz_pauseerror_code)
                    .msg(QuartzMsgUtils.quartz_pauseerror_msg);
        }
        return resultMap.code(QuartzMsgUtils.quartz_success_code)
                .msg(QuartzMsgUtils.quartz_success_msg);
    }

    /**
     * 恢复暂停的定时任务
     * @return
     */

    @RequestMapping(value = "/pause",method = {RequestMethod.POST})
    public ResultMap resumeJob(@RequestBody JobEntity jobEntity){

        if(org.springframework.util.StringUtils.isEmpty(jobEntity.getJobName())|| org.springframework.util.StringUtils.isEmpty(jobEntity.getJobGroup())){
            return resultMap.code(QuartzMsgUtils.quartz_empty_code)
                    .msg(QuartzMsgUtils.quartz_empty_msg);
        }

        try {
            quartzService.resumeJob(jobEntity.getJobName(),jobEntity.getJobGroup());
        }catch (Exception e){
            return resultMap
                    .code(QuartzMsgUtils.quartz_starerror_code)
                    .msg(QuartzMsgUtils.quartz_starerror_msg);
        }

        return resultMap.code(QuartzMsgUtils.quartz_success_code)
                .msg(QuartzMsgUtils.quartz_success_msg);
    }


    /**
     * 删除定时任务
     * @return
     */
    @RequestMapping(method = {RequestMethod.DELETE})
    public ResultMap del(@RequestBody JobEntity jobEntity){

        if(org.springframework.util.StringUtils.isEmpty(jobEntity.getJobName())||org.springframework.util.StringUtils.isEmpty(jobEntity.getJobGroup())||org.springframework.util.StringUtils.isEmpty(jobEntity.getTriggerName())||org.springframework.util.StringUtils.isEmpty(jobEntity.getTriggerGroupName())){
            return resultMap.code(QuartzMsgUtils.quartz_empty_code)
                    .msg(QuartzMsgUtils.quartz_empty_msg);
        }

        try {
            quartzService.removeJob(jobEntity.getJobName(), jobEntity.getJobGroup(), jobEntity.getTriggerName(), jobEntity.getTriggerGroupName());
        }catch (Exception e){
            return resultMap
                    .code(QuartzMsgUtils.quartz_delerror_code)
                    .msg(QuartzMsgUtils.quartz_delerror_msg);
        }

        return resultMap.code(QuartzMsgUtils.quartz_success_code)
                .msg(QuartzMsgUtils.quartz_success_msg);

    }

    /**
     * 获取Job集合对象
     * @return
     * @throws SchedulerException
     */
    private List<JobEntity> getSchedulerJobInfo() throws SchedulerException {
        List<JobEntity> jobInfos = new ArrayList<JobEntity>();
        List<String> triggerGroupNames = quartzScheduler.getTriggerGroupNames();
        for (String triggerGroupName : triggerGroupNames) {
            Set<TriggerKey> triggerKeySet = quartzScheduler
                    .getTriggerKeys(GroupMatcher
                            .triggerGroupEquals(triggerGroupName));
            for (TriggerKey triggerKey : triggerKeySet) {
                Trigger t = quartzScheduler.getTrigger(triggerKey);
                if (t instanceof CronTrigger) {
                    CronTrigger trigger = (CronTrigger) t;
                    JobKey jobKey = trigger.getJobKey();
                    JobDetail jd = quartzScheduler.getJobDetail(jobKey);
                    JobEntity jobInfo = new JobEntity();
                    jobInfo.setJobName(jobKey.getName());
                    jobInfo.setJobGroup(jobKey.getGroup());
                    jobInfo.setTriggerName(triggerKey.getName());
                    jobInfo.setTriggerGroupName(triggerKey.getGroup());
                    jobInfo.setCronExpr(trigger.getCronExpression());
                    jobInfo.setNextFireTime(trigger.getNextFireTime());
                    jobInfo.setPreviousFireTime(trigger.getPreviousFireTime());
                    jobInfo.setStartTime(trigger.getStartTime());
                    jobInfo.setEndTime(trigger.getEndTime());
                    jobInfo.setJobClass(jd.getJobClass().getCanonicalName());
                    // jobInfo.setDuration(Long.parseLong(jd.getDescription()));
                    Trigger.TriggerState triggerState = quartzScheduler
                            .getTriggerState(trigger.getKey());
                    jobInfo.setJobStatus(triggerState.toString());// NONE无,
                    // NORMAL正常,
                    // PAUSED暂停,
                    // COMPLETE完全,
                    // ERROR错误,
                    // BLOCKED阻塞
                    JobDataMap map = quartzScheduler.getJobDetail(jobKey)
                            .getJobDataMap();
                    if (null != map&&map.size() != 0) {
                        jobInfo.setCount(Integer.parseInt((String) map
                                .get("count")));
                        jobInfo.setJobDataMap(map);
                    } else {
                        jobInfo.setJobDataMap(new JobDataMap());
                    }
                    jobInfos.add(jobInfo);
                }
            }
        }
        return jobInfos;
    }


}

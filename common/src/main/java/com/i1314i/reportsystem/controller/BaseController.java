package com.i1314i.reportsystem.controller;

import com.alibaba.fastjson.JSON;
import com.i1314i.reportsystem.utils.jedisUtils.IJedisClient;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 平行时空
 * @created 2018-09-17 22:13
 **/
@Controller
public class BaseController {
    @Autowired
    private IJedisClient jedisClient;

    /**
     * 获取token对应对象
     * @param clazz
     * @param request
     * @param <T>
     * @return
     */
    public <T> T getToken(Class<T> clazz,HttpServletRequest request){
        String token=request.getHeader("token");
        T user=null;
        try{
            if(jedisClient.exists(token)){
                user= JSON.parseObject(jedisClient.get(token),clazz);
            }
        }catch (Exception e){
            return null;
        }

        return user;
    }
}

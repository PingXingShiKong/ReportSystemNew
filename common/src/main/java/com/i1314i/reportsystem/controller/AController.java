package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.po.ResultMap;
import com.i1314i.reportsystem.service.user.UserService;
import com.i1314i.reportsystem.utils.jedisUtils.IJedisClient;
import com.i1314i.reportsystem.utils.jedisUtils.JedisClient;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author 平行时空
 * @created 2018-09-07 21:25
 **/
@RestController
@RequestMapping(value = "/public")
// 拥有 admin 角色可以访问
@RequiresRoles("admin")
public class AController extends BaseController {

    @Autowired
    UserService userService;





    // 拥有 student 或 admin 角色可以访问
    @RequiresRoles(logical = Logical.OR, value = {"student", "admin"})
    @RequestMapping(value = "/test")
    public String test() throws IOException {
     //userService.tests();
        System.out.println("success");
        return "success";
    }





    // 拥有 vip 和 normal 权限可以访问
    @RequiresPermissions(logical = Logical.AND, value = {"vip", "normal"})
    public void othrt(){

    }

    // 拥有 user 或 admin 角色，且拥有 vip 权限可以访问
    @GetMapping("/getVipMessage")
    @RequiresRoles(logical = Logical.OR, value = {"admin", "school"})

    @RequiresPermissions("vip")
    public ResultMap getVipMessage() {
        return null;
    }

    public static void main(String[] args) {
        IJedisClient jedisClient=new JedisClient();
//        jedisClient.set("test01","ss",100);
//        jedisClient.set("test02","ss",100);
//        jedisClient.flushlikekey("test*");
        jedisClient.flushlikekey_foreach("test*");
    }


    public void testCustomRealm() {
        //1、获取SecurityManager工厂，此处使用Ini配置文件初始化SecurityManager
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-realm.ini");

        //2、得到SecurityManager实例 并绑定给SecurityUtils
        org.apache.shiro.mgt.SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        //3、得到Subject及创建用户名/密码身份验证Token（即用户身份/凭证）
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("zhang", "123");

        try {
            //4、登录，即身份验证
            System.out.println("登陆成功");
            subject.login(token);
        } catch (AuthenticationException e) {
            //5、身份验证失败
            System.out.println("登陆失败");
            e.printStackTrace();
        }


        //6、退出
        subject.logout();
    }

}

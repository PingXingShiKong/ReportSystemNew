package com.i1314i.reportsystem.service.user;

import com.i1314i.reportsystem.po.AdminUser;
import com.i1314i.reportsystem.po.ResultMap;
import com.i1314i.reportsystem.po.User;

import java.io.IOException;

/**
 * @author 平行时空
 * @created 2018-09-18 19:51
 **/
public interface UserService {
    /**
     * 按用户名查找user
     * @param username
     * @return
     */
    User selectUserByUserName(String username);

    /**
     * 按用户名查找admin user
     * @param username
     * @return
     */
    AdminUser selectAdminUserByUserName(String username);
    /**
     * 用户登陆
     * @param user
     * @return
     */
    ResultMap login(User user) throws IOException;

    ResultMap regUser(User user);




    void tests()throws IOException ;
}

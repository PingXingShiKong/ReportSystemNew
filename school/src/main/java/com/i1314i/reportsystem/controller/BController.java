package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.mapper.RoleMapper;
import com.i1314i.reportsystem.po.Role;
import com.i1314i.reportsystem.utils.common.TemplateUtils;
import com.i1314i.reportsystem.utils.jedisUtils.IJedisClient;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 平行时空
 * @created 2018-09-07 21:27
 **/

@RestController
    @RequiresRoles("admin")
public class BController extends BaseController {
    IJedisClient jedisClient;
    @Autowired
    RoleMapper roleMapper;
    @RequestMapping(value = "/test2")

    public String test(HttpServletRequest request){
//        Role role4=getToken(Role.class,request);
//        Role role=new Role(TemplateUtils.uuid(),"user","normal");
//        Role role2=new Role(TemplateUtils.uuid(),"admin","teacher");
//        roleMapper.insertRole(role);
//        roleMapper.insertRole(role2);
        return "successB";
    }
}
